-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Mer 12 Juillet 2017 à 17:23
-- Version du serveur :  10.1.21-MariaDB
-- Version de PHP :  5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `exercice_3`
--

-- --------------------------------------------------------

--
-- Structure de la table `movies`
--

CREATE TABLE `movies` (
  `id` int(11) NOT NULL,
  `title` varchar(30) NOT NULL,
  `actors` varchar(100) NOT NULL,
  `director` varchar(30) NOT NULL,
  `producer` varchar(30) NOT NULL,
  `year_of_prod` year(4) NOT NULL,
  `language` varchar(11) NOT NULL,
  `category` varchar(11) NOT NULL,
  `storyline` text NOT NULL,
  `video` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `movies`
--

INSERT INTO `movies` (`id`, `title`, `actors`, `director`, `producer`, `year_of_prod`, `language`, `category`, `storyline`, `video`) VALUES
(1, 'film1', 'blablablabla', 'blablablabla', 'blablablabla', 2014, 'Anglais', 'Horreur', 'blablablablablablablablablablablabla', 'https://dev'),
(2, 'film2', 'fdsgdfgdfgfdg', 'fdsgdfgdfgfdg', 'fdsgdfgdfgfdg', 2015, 'Espagnol', 'Horreur', 'dffffffffhttps://developer.mozilla.org/fr/docs/Web/HTML/Element/selecthttps://developer.mozilla.org/fr/docs/Web/HTML/Element/select', 'https://dev'),
(3, 'Titanic', 'leonardo', 'Cameron', 'Jesaispas', 1991, 'Chinois', 'Romantique', 'Film a la con', 'https://www'),
(4, 'ddddddddddddd', 'dddddddddddd', 'dddddddddddd', 'dddddddddddd', 2017, 'Chinois', 'Romantique', 'ddddddddddddddddddddddddddddddddddd', 'https://www'),
(5, 'aaaaaaaaaaaaa', 'aaaaaaaaaa', 'aaaaaaaaaaaaaaa', 'aaaaaaaaaaaaa', 2013, 'Anglais', 'Drame', 'pas de description', 'https://www.youtube.com/watch?v=-BKP98k3fFE');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `movies`
--
ALTER TABLE `movies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
