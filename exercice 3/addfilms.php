<?php
include_once 'init.php';


// Definition des variables par défaut

$title = null;
$actors = null;
$director = null;
$producer = null;
$year_of_prod = null;
$language = null;
$category = null;
$storyline = null;
$video = null;

// On controle si l'utilisateur envois le formulaire d'inscription



if (isset($_POST['sendfilm'])) {
    // Récuperation des données du formulaire (et verification si tous les champs sont présents à l'envoi)
      $title = isset($_POST['title']) ? trim($_POST['title']) : null;
      $actors = isset($_POST['actors']) ? $_POST['actors'] : null;
      $director = isset($_POST['director']) ? trim($_POST['director']) : null;
      $producer = isset($_POST['producer']) ? trim($_POST['producer']) : null;
      $year_of_prod = isset($_POST['year_of_prod']) ? trim($_POST['year_of_prod']) : null;
      $language = isset($_POST['language']) ? trim($_POST['language']) : null;
      $category = isset($_POST['category']) ? trim($_POST['category']) : null;
      $storyline = isset($_POST['storyline']) ? trim($_POST['storyline']) : null;
      $video = isset($_POST['video']) ? trim($_POST['video']) : null;

      // Peut on enregistrer les données du formulaire dans la BDD ?
      // Par défaut : OUI
      // On surchargera avec la valeur FALSE, dans le cas ou le controle du
      // formulaire détecte une erreur.
      $send = true;

      // controle de la longueur des champs titres, producteur, acteurs, realisateur et synopsis
      if (strlen($title) < 5) {
          $send = false;
          echo "<p style=\"color:red\">le Titre doit comporter plus de 5 caractères</p>";
      } else if (strlen($director) < 5) {
          $send = false;
          echo "<p style=\"color:red\">le nom du réalisateur doit comporter plus de 5 caractères</p>";
      } else if (strlen($actors) < 5) {
          $send = false;
          echo "<p style=\"color:red\">le nom des acteurs doit comporter plus de 5 caractères</p>";
      } else if (strlen($producer) < 5) {
          $send = false;
          echo "<p style=\"color:red\">le nom du producteur doit comporter plus de 5 caractères</p>";
      } else if (strlen($storyline) < 5) {
          $send = false;
          echo "<p style=\"color:red\">le synopsis doit comporter plus de 5 caractères</p>";
      }

      // controle du lien de la bande annonce
      if(!filter_var($video, FILTER_VALIDATE_URL)) {
          $send = false;
          echo "<p style=\"color:red\">Le lien fourni n'est pas valide</p>";
}


    // - Controle l'existance du film dans la BDD
    // -> Le titre du film ne doit pas etre présent dans la BDD
    if ($send) {
        $q = "SELECT id FROM `movies` WHERE title=:title";
        $q = $pdo->prepare($q);
        $q->bindValue(":title", $title, PDO::PARAM_STR);
        $q->execute();
        if ($q->fetch(PDO::FETCH_OBJ)) {
        // if (true) {
            $send = false;
            echo "<p style=\"color:red\">Un film est déjà enregistré avec le nom $title.</p>";
        }
        $q->closeCursor();
    }

    if ($send) {
        // Enregistrement du film
        $q = "INSERT INTO `movies` (`title`,`actors`,`director`,`producer`,`year_of_prod`,`language`,`category`,`storyline`,`video`)
                            VALUES (:title, :actors, :director, :producer, :year_of_prod, :language, :category, :storyline,:video)";
        $q = $pdo->prepare($q);
        $q->bindValue(":title", $title, PDO::PARAM_STR);
        $q->bindValue(":actors", $actors, PDO::PARAM_STR);
        $q->bindValue(":director",  $director, PDO::PARAM_STR);

        $q->bindValue(":producer", $producer, PDO::PARAM_STR);
        $q->bindValue(":year_of_prod", $year_of_prod, PDO::PARAM_INT);
        $q->bindValue(":language", $language, PDO::PARAM_STR);
        $q->bindValue(":category", $category, PDO::PARAM_STR);
        $q->bindValue(":storyline", $storyline, PDO::PARAM_STR);
        $q->bindValue(":video", $video, PDO::PARAM_STR);
        $q->execute();
        $q->closeCursor();
        echo "<p style=\"color:blue\">Votre film a bien été ajouté</p>";
        echo "<a href=\"index.php\">Retour à la liste des films</a>";
    }




}
?>




 <!DOCTYPE html>
 <html>
     <head>
         <meta charset="utf-8">
         <title>Ajouter un Film</title>
     </head>
     <body>

         <h1>Ajouter un film</h1>


         <form  method="post">

             <div>
                 <label for="title">Nom du film</label><br>
                 <input type="text" id="title" name="title" value="<?php echo $title; ?>">
             </div>
             <div>
                 <label for="actors">Noms des acteurs</label><br>
                 <input type="text" id="actors" name="actors" value="<?php echo $actors; ?>">
             </div>

             <div>
                 <label for="director">Nom du réalisateur</label><br>
                 <input type="text" id="director" name="director" value="<?php echo $director; ?>">
             </div>

             <div>
                 <label for="producer">Nom du producteur</label><br>
                 <input type="text" id="producer" name="producer" value="<?php echo $producer; ?>">
             </div>

             <div>
                 <label for="year_of_prod">Année : </label>
                 <select name="year_of_prod" value="<?php echo $year_of_prod; ?>">
                   <option value=""></option>
                   <?php for($i= date('Y'); $i>=date('Y')-100; $i--): ?>
                   <option value="<?php echo $i; ?>"><?php
                    echo $i;
                    ?></option>
                   <?php endfor; ?>
                 </select>
             </div>
             <div>
                 <label for="language">Langue :</label>
                 <select  name="language" value="<?php echo $language; ?>">
                   <option>----</option>
                   <option>Francais</option>
                   <option>Anglais</option>
                   <option>Espagnol</option>
                   <option>Chinois</option>

                 </select>
             </div>

             <div>
                 <label for="category">Catégorie :</label>
                 <select name="category" value="<?php echo $category; ?>">
                   <option>----</option>
                   <option>Humour</option>
                   <option>Drame</option>
                   <option>Romantique</option>
                   <option>Horreur</option>
                 </select>
             </div>

             <div>
                 <label for="storyline">Synopsis</label><br>
                 <textarea type="text" id="storyline" name="storyline" cols="80" rows="8"value=""></textarea>
             </div>

             <div>
                 <label for="video">Lien de la bande annonce :</label><br>
                 <input type="text" id="video" name="video" value="<?php echo $video; ?>">
             </div>

             <button type="submit" name="sendfilm">Envoyer</button>





         </form>

     </body>
 </html>
