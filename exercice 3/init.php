<?php
include_once 'config.php';



// --------------------
// INITIALISATION DE SESSION
// --------------------
session_start();

// --------------------
// CONNEXION BDD
// --------------------

// On test la connexion à la BDD
try {
    // Création de la connexion à la base de données
    $pdo = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $user, $pass);
}
// Si la connexion échoue, on attrape l'exception (message d'erreur)
// et on arrete l'execution du programme
catch (Exception $e) {
    die('Erreur : ' . $e->getMessage());
}
