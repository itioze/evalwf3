
<?php
include_once 'init.php';

$query_string = "SELECT id, title, director, year_of_prod FROM movies";

$queryPDO = $pdo->query($query_string);

    $results = $queryPDO->fetchAll(PDO::FETCH_OBJ);


?>


 <!DOCTYPE html>
 <html>
     <head>
         <meta charset="utf-8">
         <title>Liste des films</title>
     </head>
     <body>
         <h1>Liste des films</h1>

         <table border="1">
             <tr>
                 <th>Nom du film</th>
                 <th>Réalisateur</th>
                 <th>Année</th>
                 <th>Plus d'infos</th>

             </tr>

             <?php foreach ($results as $film): ?>
             <tr>
                 <td><?php echo $film->title ; ?></td>
                 <td><?php echo $film->director; ?></td>
                 <td><?php echo $film->year_of_prod; ?></td>
                 <td><?php echo "<a href=\"film.php?filmId=".$film->id."\">Plus d'infos</a>" ?></td>


             </tr>
             <?php endforeach; ?>
         </table>

             <a href="addfilms.php">Ajouter un film</a>

     </body>
 </html>
