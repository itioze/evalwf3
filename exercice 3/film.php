<?php



include_once 'init.php';

// On teste l'existance du paramètre "filmId" dans l'url.
// Si celui-ci n'est pas définie, on arrete le programme
if (!isset($_GET['filmId'])) {
    die("le paramètre filmId est manquant.");
}

$id_film = $_GET['filmId'];

// On définie la chaine de caractere de la requete
$query_string = "SELECT
        id,
        title,
        director,
        producer,
        year_of_prod,
        language,
        category,
        storyline,
        video
    FROM
        `movies`

    WHERE id=:idFilm";

$queryPDO = $pdo->prepare($query_string);
$queryPDO->bindValue(":idFilm", $id_film, PDO::PARAM_INT);
$queryPDO->execute();

// On récupère le resultat de la requete
$result = $queryPDO->fetchAll(PDO::FETCH_OBJ);


// On affiche le nom du film
$film_title = $result[0]->title;
echo "<h1>".$film_title."</h1>";

// On affiche le réal du film
$film_director = $result[0]->director;
echo "Réalisateur: ".$film_director."<br>"    ;

// On affiche le producteur du film
$film_producer= $result[0]->producer;
echo "Producteur: ".$film_producer."<br>" ;

// On affiche l'année de production du film
$film_year_of_prod= $result[0]->year_of_prod;
echo "Année de production : ".$film_year_of_prod."<br>";

// On affiche le langue du film
$film_language= $result[0]->language;
echo "Langue : ".$film_language."<br>";

// On affiche la category du film
$film_category= $result[0]->category;
echo "Catégorie: ".$film_category."<br>";

// On affiche la category du film
$film_storyline= $result[0]->storyline;
echo "Synopsis: ".$film_storyline."<br>";

// On affiche la category du film
$film_video= $result[0]->video;
echo "Bande annonce: "."<a href=\"".$film_video."\">".$film_video."</a><br>";




?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Page film</title>
    </head>
    <body>
        <a href="index.php">Retour à la liste des films</a>
    </body>
</html>
