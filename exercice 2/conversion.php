<?php

// définition de la variable par defaut (avant passage en POST)
$euro = null;

// Fonction de conversion des euros en dollars
if (isset($_POST['validate'])) {




    // définition de la variable $euro qui contiendra la valeur de $_POST['euro'] si elle est défini ou à defaut sera nulle.
    $euro = isset($_POST['euro']) ? trim($_POST['euro']) : null;

    //controle si la donnée entrée est vide et est bien un chiffre
    if(empty($euro)) {
        echo "Indiquez un montant en euros";
        header("refresh:5;url=conversion.php");
    }
    else if (!is_numeric($euro)) {
        echo "Indiquez un chiffre";
        header("refresh:5;url=conversion.php");
    }
    else if ($euro < 0) {
        echo "Indiquez un chiffre positif";
        header("refresh:5;url=conversion.php");
    }

    // fonction avec deux parametres
    function convert($euros,$exchange)
    {
        $euros = $euros*$exchange;
        return $euros;
    }
    // Stockage du resultat de la variable dans la variable $convert (qui sera appelée plus tard)
    $convert = convert($_POST['euro'],1.1457);

}

?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <title>Conversion</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <meta name="description" content="">
  </head>
    <body>



    <h2>Convertisseur des euros en dollars</h2>

    <form method="POST" name="convertEurUsd">
        <input type="text" name="euro" id="valeur" placeholder="Indiquez un montant en euros">
        <input type="submit" name="validate" value="Convertir">
    </form>

    <?php // Affichage d'un resultat uniquement si les controles ont été positifs
        global $convert;
        if (isset($convert) && is_numeric($euro) && ($euro>0) ) {
            echo $euro. "€ = ". $convert."$";
        }

    ?>

    </body>
</html>
