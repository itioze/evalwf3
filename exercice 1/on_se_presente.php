<?php

// Tableau en PHP des informations

$tableauPresentation = array(
        "Prénom" => "Quentin",
        "Nom"=> "Belvalette",
        "Addresse" => "123 london street",
        "Code postal" => "12587",
        "Ville"=> "London",
        "email"=> "bruce@wayne-enterprise.com",
        "Téléphone" => "0112457836",
        "Date de naissance"=> "1987-04-05"

);



?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>On se présente</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    </head>
    <body>

        <h1>On se présente</h1>

            <ul> <!-- Boucle qui parcourt le tableau pour en afficher les données-->

              <?php foreach ($tableauPresentation as $key => $value) : ?>
                  <li>
                      <?php if( $key == "Date de naissance") {
                          $date = new DateTime($value);
                          echo $key." : ". $date->format('d/m/Y');
                      } else {
                          echo $key." : ". $value;
                      } ?>
                  </li>
          <?php endforeach; ?> <!-- fin de la boucle -->
            </ul>

    </body>
</html>
